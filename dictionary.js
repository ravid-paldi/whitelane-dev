var AGGREGATE_GENERIC_VENDORS_LIST = [
  "Other 1 (please specify)",
  "Other 2 (please specify)",
  "Other 3 (Canada)"
  //      "Accenture"
];
var dic_GenSat = {
  "Very unsatisfied": 0,
  "Unsatisfied": 0.2,
  "Somewhat unsatisfied": 0.4,
  "Somewhat satisfied": 0.6,
  "Satisfied": 0.8,
  "Very satisfied": 1
};

var dic_GenSat_ducplicate = {
  "Very unsatisfied": "Very unsatisfied",
  "Unsatisfied": "Unsatisfied",
  "Somewhat unsatisfied": "Somewhat unsatisfied",
  "Somewhat satisfied": "Somewhat satisfied",
  "Satisfied": "Satisfied",
  "Very satisfied": "Very satisfied"
};

var dic_Renewal = {
  "(Almost) certainly not": 0,
  "Probably not": 0.33,
  "Probably": 0.66,
  "(Almost) certainly": 1
};
var dic_procure = {
  "First negotiate with the existing vendor":"First negotiate with the existing vendor",
  "Directly run a competitive process":"Directly run a competitive process",
  "N/A - I'm not sure":"N/A - I'm not sure"
};
  
var dic_KPI = ["Service Delivery",
  "Cloud capability",
  "Account Mgmt",
  "Pro-activity",
  "Price Level",
  "Contractual Flexibility",
  "Transformation",
  "Innovation",
  "Business Understanding"
];

var ITDomain = {"am.Value":"am","im.Value":"im","telco.Value":"telco","euc.Value":"euc"};

var TAB_NAME_suffix = [".Count", ".AvgGenSat",".GenSatDetail",".Renewal"];

var dic_OutsourcingPlans = {
  "We will outsource more":"We will outsource more",
  "There will be no change":"There will be no change",
  "We will outsource less":"We will outsource less",
  "We don't know yet":"We don't know yet"
};

var dic_Goals = {
  "Not achieved":"Not achieved",
  "Partly achieved":"Partly achieved",
  "Achieved":"Achieved",
  "More than achieved":"More than achieved",
  "N/A - I don't know":"N/A - I don't know"
};

var GovernanceAssessment_list = ["items:",
  "Sourcing strategy",
  "Supplier selection and contracting",
  "Transition & change management",
  "Service management / service integration",
  "Supplier relationship and contract management",
  "Business relationship management / demand forecasting"
];

var OutsourcingDrivers_list = ["items",
  "Focus on core business",
  "Cost reduction",
  "Improvement of service quality",
  "Access to resources",
  "More transparency on costs",
  "More financial flexibility",
  "Innovation",
  "Business transformation",
  "Other"
];

var AgileRoboticsAI_col_Names = {'Agile' : {
                                              "We are already using Agile as the preferred model": "We are already using Agile as the preferred model",
                                              "Not yet, but we plan to use Agile": "Not yet, but we plan to use Agile",
                                              "No, we don't use Agile and do not plan to": "No, we don't use Agile and do not plan to",
                                              "We don't know yet": "We don't know yet"  
                                            },
                                'Robotics': {
                                              "Yes, and we are further increasing the use": "Yes, and we are further increasing the use",
                                              "Yes, and the usage remains as it is": "Yes, and the usage remains as it is",
                                              "No, but we are planning to use it": "No, but we are planning to use it",
                                              "No, and we don't have any plans to use it": "No, and we don't have any plans to use it",
                                              "We don't know yet": "We don't know yet"
                                            },
                                'RoboticsBenefits' : {
                                              'Very significant': 'Very significant',
                                              'Significant': 'Significant',
                                              'Moderate': 'Moderate',
                                              'Marginal': 'Marginal',
                                              'N/A - I don\'t know': 'N/A - I don\'t know'
                                            },
                                'DigitalBusinessTransformation' : ''
};
