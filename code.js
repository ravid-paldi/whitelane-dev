//Shira - this is the sheet to populate graph for im.VALUE = 1
var ImValue_spreadsheet = "1ujfolbLB1DXMSeD_CCUlH3Wwn8O9sKTp5r4mua-lB3w";
var RawDataorg_TAB_NAME = "Raw Data org";
var RawDataOrgSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(RawDataorg_TAB_NAME);
var RAW_DATA_ORG_LEN = RawDataOrgSheet.getLastRow();
var RAW_DATA_ORG_COL_CNT = RawDataOrgSheet.getLastColumn();
// Section 1 +2
var ORG_INDUSTRY_COL_NAME ="Industry Overall";
var G_ORG_SECTOR_TAB_NAME = "W: Evaluations Per Sector";
var OrgSectorGrapghSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(G_ORG_SECTOR_TAB_NAME);
var OUT_PLANS_COL_NAME = "OutsourcingPlans";
var G_OUTS_PLANS_TAB_NAME = "W: Outsourcing Plans";
var OutsPlansGrapghSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(G_OUTS_PLANS_TAB_NAME);
var OUT_DRIVERS_COL_NAME ="OutsourcingDrivers", OUT_DRIVERS_COL_COUNT = 8;
var G_OUTS_DRIVERS_TAB_NAME = "W: Outsourcing Drivers";
var OutsDriverGrapghSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(G_OUTS_DRIVERS_TAB_NAME);
var OBJ_ACHIEVE_COL_NAME = "OutsourcingDriversAchieved";
var G_OBJ_ACHIEVE_TAB_NAME = "W: Objectives Achieve";
var ObjAchieveGraphSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(G_OBJ_ACHIEVE_TAB_NAME);
var GOV_CAP_COL_NAME ="GovernanceAssessment", GOV_CAP_COL_COUNT = 6;
var G_GOV_CAP_TAB_NAME = "W: Governance Capability";
var GovCapabilityGrapghSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(G_GOV_CAP_TAB_NAME);
var CLOUD_COL_NAME ="CloudBarriers", CLOUD_COL_COUNT = 10;
var G_CLOUD_TAB_NAME = "W: Cloud Transition";
var CloudBarrGraphSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(G_CLOUD_TAB_NAME);
var G_AgileRobo_TAB_NAME = "W: Agile/Robotics/AI/DIG";
var AgileRoboGraphSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(G_AgileRobo_TAB_NAME);

 //Section 3 
var GENERIC_VENDOR_NAME = "Anonymous Vendor";
var RAW_DATA_COL_CNT = 22;
var EVALUATIONS_TAB_NAME = "Hidden_Evaluations";
var GENSAT_COL_NAME = "Gen Sat";
var VENDORS_COL_NAME = "Vendor";
var VENDORS_TAB_NAME = "Hidden_Vendors";
var RAW_DATA_TAB_NAME = "Raw Data";
var PROCUREMENT_COL_NAME = "procureValue";
var RENEWAL_COL_NAME = "reletValue";
var G_OVGENSAT_TAB_NAME = "W: Overall General Satisfaction";
var G_EVALUATIONS_PER_VENDOR_TAB_NAME = "W: Evaluations Per Vendor";
var G_AVG_SAT_PER_VENDOR_TAB_NAME = "W: Avg Satisfaction Per Vendor";
var G_KPI_TAB_NAME = "D: KPI model",
    GRAPH_KPI_TAB_NAME = "W: KPI model";
//shira - added 3 more sheets for slides 27-29
var G_NEGOTIATION_TAB_NAME = "W: Negotiation";
var G_RENEWAL_TAB_NAME = "W: Renewal";
var G_GENSAT_BY_PROVIDER_TAB_NAME = "W: GenSat by provider";
var OvGenSatGrapghSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(G_OVGENSAT_TAB_NAME);
var EvaluationsVendorGraphSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(G_EVALUATIONS_PER_VENDOR_TAB_NAME);
var AvgSatVendorGrapghSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(G_AVG_SAT_PER_VENDOR_TAB_NAME);
var VendorsSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(VENDORS_TAB_NAME);
var RawDataSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(RAW_DATA_TAB_NAME);
var EvaluationsSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(EVALUATIONS_TAB_NAME);
var KPISheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(G_KPI_TAB_NAME);
var GraphKPISheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(GRAPH_KPI_TAB_NAME);
var RAW_DATA_LEN = RawDataSheet.getLastRow();
var vednorsColIndex = getColIndexByName(RawDataSheet, VENDORS_COL_NAME);
var T_KPI_marktAVG_TAB_NAME = "T: Market average";
var KPITableSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(T_KPI_marktAVG_TAB_NAME);
var MainThresh = 8;
var IMThresh = 7;


function onOpen() {
    var spreadsheet = SpreadsheetApp.getActive();
    var menuItems = [{
        name: 'RefreshData',
        functionName: 'refreshData'
    }];
    spreadsheet.addMenu('Presentations Helper', menuItems);
}

function refreshData() {
    Logger.log(">>>>>>> Refreshing Data -----");
    promptUser('Threshold for Overall Data', "Please enter a number ", setMainThresh);
    promptUser('Threshold for Im/Am/Telco/Euc Values?', "Please enter a number ", setIMThresh);
  
    //Shira - passing the Evaluations table from refreashEvaluations() to refreashVendors() 
    var EvaluationsData = refreashEvaluations();
    refreashVendors(EvaluationsData);
    refreashORG();
    Logger.log("----- Finished Refreshing Data <<<<<<");
}

function refreashEvaluations() {
    var newValues = [];
    Logger.log("----- starting loop Refreshing Evaluations <<<<<<");
    for (var d = 1; d <= RAW_DATA_LEN; d++) {
        // doublecheck that the row is not hidden by a filter. 
        if (!RawDataSheet.isRowHiddenByFilter(d)) {
            // Apply the "Other" aggregation requirement
            var newRow = RawDataSheet.getRange(d, 1, 1, RAW_DATA_COL_CNT).getValues()[0];
            if (AGGREGATE_GENERIC_VENDORS_LIST.indexOf(newRow[vednorsColIndex - 1]) != -1) {
                newRow[vednorsColIndex - 1] = GENERIC_VENDOR_NAME
            };
            newValues.push(newRow);
        }
    }
    EvaluationsSheet.clear();
    EvaluationsSheet.getRange(1, 1, newValues.length, newValues[0].length).setValues(newValues);
    return newValues;
    Logger.log("----- Finished Refreshing Evaluations <<<<<<");
}

function refreashVendors(evalData) {
    Logger.log(">>>>>>> Refreshing Vendors -----");
    VendorsSheet.clear();
    var vendorColIndex = getColIndexByName_data(evalData,VENDORS_COL_NAME);
    var gensatColIndex = getColIndexByName_data(evalData,GENSAT_COL_NAME);
    var renewalColIndex = getColIndexByName_data(evalData,RENEWAL_COL_NAME);
    var procureColIndex = getColIndexByName_data(evalData,PROCUREMENT_COL_NAME);
    
    //Shira - Creating hidden vendors from evaluation data
    var vendorHidden = countAvg_HiddenVendors(evalData);
    VendorsSheet.getRange(1, 1,vendorHidden.length,vendorHidden[0].length).setValues(vendorHidden);
    
    // *-* // Generating Data for pie chart widget: Amount of Evaluations Per Vendor (**soreted with Other inside**) - slide 18
    EvaluationsVendorGraphSheet.clear();
    var EvalVendor = EvalPerVendor(vendorHidden);
    EvaluationsVendorGraphSheet.getRange(1, 1, EvalVendor.length, EvalVendor[0].length).setValues(EvalVendor);
    //formating colors
    PieVendorCountChart(EvaluationsVendorGraphSheet);
    
    // *-* // Generating Data for pie chart widget: Overall General Satisfaction - slide 19
    //shira - deleted the google function and added the values from the dictionary
    OvGenSatGrapghSheet.clear();
    var GeneralSatchart = GenSatGeneral(evalData,gensatColIndex,dic_GenSat,"number");
    OvGenSatGrapghSheet.getRange(1, 1, GeneralSatchart.length, GeneralSatchart[0].length).setValues(GeneralSatchart);
    //formating colors
    GenSatChartColors(OvGenSatGrapghSheet);

    // *-* // Generating Data for bar chart widget: Avg Satisfaction Per Vendor - slide 20
    AvgSatVendorGrapghSheet.clear();
    var filteredVendors = VendorList(vendorHidden).FilterVendors; 
    AvgSatVendorGrapghSheet.getRange(1, 1, filteredVendors.length, filteredVendors[0].length).setValues(filteredVendors);
    //formating colors
    BarChart(AvgSatVendorGrapghSheet);
    MinMaxBarChart(AvgSatVendorGrapghSheet,filteredVendors,0);
    
    // using only colums 1 and 3 to create the base for the KPI_table
    var KPI_table = filteredVendors.map(function(item){ 
      return [item[0],item[2]];
     });
    AvgSatVendorGrapghSheet.getRange(1, 5, 2).setValues(average(KPI_table));
    KPI_table.shift();
    var KPIAvg = KPI_table.map(function(item){return item[1];}).reduce(function (a, b){
        return (a + b );
     },0) / KPI_table.length;
    KPI_table.unshift(["Vendor", 'GenSat']);
    KPI_table.push(['AVERAGE', KPIAvg])
    //Shira - Getting sorted and filtered list of vendors by gensat
    var sortedVendorList = VendorList(vendorHidden).FilterVendorsList;
    
    // *-* // Generating Data for 100% stack bar chart widget: General satisfaction by service provider - slide 27
    //Shira - Populationg sheet "W: Avg Satisfacatiton Per Vendor" 
    var targetChartGen = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(G_GENSAT_BY_PROVIDER_TAB_NAME);
    getPivotArray_precent(filterarray(evalData, vendorColIndex, gensatColIndex), sortedVendorList, dic_GenSat, targetChartGen);
    //formating colors
    GenSatChartColors(targetChartGen);

    // *-* // Generating Data for 100% stack bar chart widget: Renewal Intentions - slide 28
    //Shira - Populationg sheet "W: Renewal"
    //Shira - fixed bug in chart colors
    var targetChartRenewl = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(G_RENEWAL_TAB_NAME);
    getPivotArray_precent(filterarray(evalData, vendorColIndex, renewalColIndex), sortedVendorList, dic_Renewal, targetChartRenewl);
    //formating colors
    RenewalChartColors(targetChartRenewl);
    
    // *-* // Generating Data for 100% stack bar chart widget: Negotiation process - slide 29
    //Shira - Populationg sheet "W: Negotiation"
    var targetChartNego = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(G_NEGOTIATION_TAB_NAME);
    getPivotArray_precent(filterarray_text(evalData, vendorColIndex, procureColIndex), sortedVendorList, dic_procure, targetChartNego);
    //formating colors
    NegotiationChartColors(targetChartNego);

    // generating KPI charts
  //Shira - removed the google formula
    KPISheet.clear();
    GraphKPISheet.clear();
    var ColumnNum = 1;
    
    dic_KPI.forEach(function(KPIitem, index) {
      var KPICol = getColIndexByName_data(evalData,KPIitem);
      //Filtering the array to the colindex of the KPI
      var KPIArray = countAvgKPI(filterarray(evalData,vendorColIndex,KPICol),sortedVendorList);
      // Calculating the avg
      var KPIAvg = KPIArray.map(function(item){
        return item[1];}).reduce(function (a, b){
          return (a + b );
        },0) / KPIArray.length;
      //Adding avg value for graph sheet 
      GraphKPISheet.getRange(1, ((index * 6) + 1), 1, 2).setValues([["KPI " + (index+1) + ": " + KPIitem, KPIAvg]]);
      //Adding title + values to KPI raw data sheet
      KPIArray.sort(compareSecondColumn);
      KPIArray.unshift(["Vendor", KPIitem]);
      KPISheet.getRange(1, ColumnNum, KPIArray.length,2).setValues(KPIArray);
      MinMaxBarChart(GraphKPISheet,KPIArray,index);
      KPIArray.push(['AVERAGE', KPIAvg])
      KPI_table.forEach(function(item1, index1) {
         for (var i = 0; i < KPIArray.length; i++) {
           if (item1[0] == KPIArray[i][0]){
               item1.push(KPIArray[i][1]);}
         };
      });
      ColumnNum += 3;
    });
    KPITableSheet.clear();
    KPITableSheet.getRange(1, 1, KPI_table.length, KPI_table[0].length).setValues(KPI_table);
  
    //*-* // POC for populating a seperate spreadsheet
    //
    setMainThresh(IMThresh);
    for (item in ITDomain){
        var ITDomaiColIndex = getColIndexByName_data(evalData,item)
        fillsheet(evalData,ITDomaiColIndex,ITDomain[item],ImValue_spreadsheet);
    };
};

function refreashORG(){
   var data_Raw_org = [];
   Logger.log("----- starting loop Refreshing Raw Data Org <<<<<<");
   for (var d = 1; d <= RAW_DATA_ORG_LEN; d++) {
        // doublecheck that the row is not hidden by a filter. 
       if (!RawDataOrgSheet.isRowHiddenByFilter(d)) {
           var newRow = RawDataOrgSheet.getRange(d, 1, 1, RAW_DATA_ORG_COL_CNT).getValues()[0];
           data_Raw_org.push(newRow);
       }
   }
   Logger.log("----- Finished Refreshing  Raw Data Org <<<<<<");
   // Section 1 
   
   // *-* // Industry by Precentage - pie chart
   OrgSectorGrapghSheet.clear();
   var industry_col_index = getColIndexByName_data(data_Raw_org,ORG_INDUSTRY_COL_NAME);
   var SectorGraph = SectorGeneral(data_Raw_org,industry_col_index);
   OrgSectorGrapghSheet.getRange(1, 1, SectorGraph.length, SectorGraph[0].length).setValues(SectorGraph);
   PieVendorCountChart(OrgSectorGrapghSheet);
   
   // Section 2
   
   // *-* // Outsourcing pans for the next 2 years - pie chart
   OutsPlansGrapghSheet.clear();
   var OutsourcingPlans = getColIndexByName_data(data_Raw_org,OUT_PLANS_COL_NAME);
   var OutsourcingPlansGraph = GenSatGeneral(data_Raw_org,OutsourcingPlans,dic_OutsourcingPlans,"text");
   OutsPlansGrapghSheet.getRange(1, 1, OutsourcingPlansGraph.length, OutsourcingPlansGraph[0].length).setValues(OutsourcingPlansGraph);
   GenSatChartColors(OutsPlansGrapghSheet);
   
   // *-* // Organizations want to achieve by outsourcing sorted percentage - bar chart
   OutsDriverGrapghSheet.clear();
   var OutsourcingDrivers = getColIndexByName_data(data_Raw_org, OUT_DRIVERS_COL_NAME+"1");
   var values = Multi_Col_Avg(data_Raw_org,OutsourcingDrivers,OUT_DRIVERS_COL_COUNT,OutsDriverGrapghSheet);
   OutsDriverGrapghSheet.getRange(1, 3, 2).setValues(average(values.FilterArray));
   MinMaxBarChart(OutsDriverGrapghSheet,values.FilterArray,0);
   BarChart(OutsDriverGrapghSheet);
   
   // *-* // degree are you currently achieving these objectives - 100% stacked bar chart
   ObjAchieveGraphSheet.clear();
   var new2DimArray = MultiCol_To_2DArray(data_Raw_org,OBJ_ACHIEVE_COL_NAME,OUT_DRIVERS_COL_COUNT,OutsourcingDrivers_list);
   getPivotArray_precent(new2DimArray, values.FilterList, dic_Goals, ObjAchieveGraphSheet);
   
   // Section 4 
   
   // *-* // how satisfied are you with the following governance capability areas?(Client perspective) - 100% stacked bar chart
   GovCapabilityGrapghSheet.clear();
   var new2DimArray = MultiCol_To_2DArray(data_Raw_org,GOV_CAP_COL_NAME,GOV_CAP_COL_COUNT,GovernanceAssessment_list);
   getPivotArray_precent(new2DimArray, GovernanceAssessment_list, dic_GenSat_ducplicate, GovCapabilityGrapghSheet);
   
   // *-* // 4 pie charts about robotic/agile/AI/digbiz
   var item_index = 1;
   AgileRoboGraphSheet.clear();
   for (key in AgileRoboticsAI_col_Names) {
     var ColIndex = getColIndexByName_data(data_Raw_org,key);
     if (AgileRoboticsAI_col_Names[key] == ''){
       var SlideData = SectorGeneral(data_Raw_org,ColIndex);}
     else {
       var SlideData = GenSatGeneral(data_Raw_org,ColIndex,AgileRoboticsAI_col_Names[key],"text");}
       
     AgileRoboGraphSheet.getRange(1, item_index, SlideData.length,2).setValues(SlideData);
     item_index+=4;
   }; 
   
   // *-* // Slide 82 - What barriers need to be overcome in order to successfully transition to the public cloud? - bar chart
   CloudBarrGraphSheet.clear()
   var CloudBarr = getColIndexByName_data(data_Raw_org, CLOUD_COL_NAME+"1");
   var values = Multi_Col_Avg(data_Raw_org,CloudBarr,CLOUD_COL_COUNT,CloudBarrGraphSheet);
   CloudBarrGraphSheet.getRange(1, 3, 2).setValues(average(values.FilterArray));
   MinMaxBarChart(CloudBarrGraphSheet,values.FilterArray,0);
   BarChart(CloudBarrGraphSheet);
   
}