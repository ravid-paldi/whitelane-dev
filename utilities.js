function setMainThresh(thresh) {
    MainThresh = thresh;
}

function setIMThresh(thresh) {
    IMThresh = thresh;
}

function average(array2D){
    array2D.shift();
    var Avg = array2D.map(function(item){
        return item[1];}).reduce(function (a, b){
          return (a + b );
        },0) / array2D.length;
    return [["Average"],[Avg]];
}

function promptUser(title, prompt, func_if_got_result) {
    var ui = SpreadsheetApp.getUi();

    var response = ui.prompt(title, prompt, ui.ButtonSet.OK_CANCEL);

    // Process the user's response.
    if (response.getSelectedButton() == ui.Button.OK) {
        var prompt_result = response.getResponseText()

        func_if_got_result(prompt_result)
    } else if (response.getSelectedButton() == ui.Button.CANCEL) {
        Logger.log('promptUser::Button.CANCEL %s', title);
    } else {
        Logger.log('promptUser::The user clicked the close button in the dialog\'s title bar. %s', title);
    }
}


function indexToLetter(column) {
    var temp, letter = '';
    while (column > 0) {
        temp = (column - 1) % 26;
        letter = String.fromCharCode(temp + 65) + letter;
        column = (column - temp - 1) / 26;
    }
    return letter;
}

function letterToIndex(letter) {
    var column = 0,
        length = letter.length;
    for (var i = 0; i < length; i++) {
        column += (letter.charCodeAt(i) - 64) * Math.pow(26, length - i - 1);
    }
    return column;
}

function getColIndexByName(sheet, name) {
    //  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(sheetName);
    var range = sheet.getDataRange();
    var evalCntColIndex = range.getLastColumn();
    var result = -1;
    for (var i = 1; i < evalCntColIndex + 1; i++) {
        var text = sheet.getRange(1, i).getValue();
        if (text == name)
            result = i;
    }
    return result;
}
//Shira - the same as the getColIndexByName but recieves an eval as an array
function getColIndexByName_data(data, name) {
    var range = data[0];
    var evalCntColIndex = range.length;
    var result = -1;
    for (var i = 0; i < evalCntColIndex; i++) {
        var text = range[i];
        if (text == name)
            result = i;
    }
    return result;
}

function getColLetterByName(sheet, name) {
    var i = getColIndexByName(sheet, name);
    var ret = "";
    if (i != -1) {
        ret = indexToLetter(i);
    }
    Logger.log(ret);
    return (ret);
}

//SECTION 1 - input text col -> filters blank text array -> output value + precent
function SectorGeneral(array,ColIndex){
    var a = filterarray_text(array,ColIndex,ColIndex);
    var result = a.reduce(function(res, value) {
    if (!res[value[0]]) {
      res[value[0]] = 0;
    }
    res[value[0]] += 1;
    return res; 
    }, {});
    var total = 0;
    var finalArray = [];
    
    for (var key in result){
      total += result[key];
    }
    for (var key in result){
      var precent = result[key]/total;
      finalArray.push([key,precent]);
    }  
  
    finalArray.sort(compareSecondColumn);
    finalArray.unshift(["Sector","Sector Percentage"]);
    return finalArray;
  };

//filters the vendor list according to abovethresh
function VendorList(data){
  Logger.log(data);
  var newarray = data.filter(aboveThresh);
  newarray.sort(compareSecondColumn).sort(compareThirdColumn);
  newarray.unshift(data[0]);
  Logger.log(newarray);
  //AvgSatVendorGrapghSheet.clear().getRange(1, 1, newarray.length, newarray[0].length).setValues(newarray);
  var myArray = newarray.map(function(item){
    return item[0];
  });
  return {FilterVendors: newarray, FilterVendorsList: myArray}
}

function compareThirdColumn(a, b) {
    if (a[2] === b[2]) {
        return 0;
    } else {
        return (a[2] > b[2]) ? -1 : 1;
    }
}

function compareSecondColumn(a, b) {
    if (a[1] === b[1]) {
        return 0;
    } else {
        return (a[1] > b[1]) ? -1 : 1;
    }
}

//functions runs on the 4th column of the hiddenvendors and check if its above threshhold
function aboveThresh(item) {
    return item[3] > 0;
}

//filters the evaluation array accourding to IM.value. AM.value
function filterEvaluation(data, filterIndex){
    var EvalData = data.filter(function(item){
      return item[filterIndex] == 1;
    });
    EvalData.unshift(data[0]);
    return EvalData;
}
  
//extracts from evaluation array the vendors and data index (preperation for countAvg() and getPivotArray_precent())
//Shira - fixed bug in data filtering
function filterarray(data, rowIndex, dataIndex) {
    var cleanData = data.filter(function(n){ 
      return n[rowIndex] != '' && typeof n[dataIndex] === 'number';
    });
    var arrayData = cleanData.map(function(item) {
      return [item[rowIndex], item[dataIndex]];
    });
    return arrayData;
};

function filterarray_text(data, rowIndex, dataIndex) {
    var cleanData = data.filter(function(n){ 
      return n[rowIndex] != '' && n[dataIndex] != '' &&  n[dataIndex] != 'N/A';
    });
    var arrayData = cleanData.map(function(item) {
      return [item[rowIndex], item[dataIndex]];
    });
    arrayData.shift();
    return arrayData;
};

function getPivotArray_precent(dataArray, VendorValues, dic, targetSheet) {
    var result = {},
        ret = [];
    var newCols = [];
    for (var i = 0; i < dataArray.length; i++) {

        if (!result[dataArray[i][0]]) {
            result[dataArray[i][0]] = {};
        }
        if (!result[dataArray[i][0]][dataArray[i][1]]) {
            result[dataArray[i][0]][dataArray[i][1]] = 1;
        } else {
            result[dataArray[i][0]][dataArray[i][1]] += 1
        }
    }
    //Logger.log(result);
    var item = [];
    var lastRow = [""];
    for (var key in dic) {
        newCols.push(key);
        lastRow.push("0.01");
    }
    //Add Header Row
    item.push('Vendor');
    item.push.apply(item, newCols);
    item.push('Total');
    lastRow.push("");
    ret.push(item);

    //Add content 
    for (var j = 1; j < VendorValues.length; j++) {
        item = [];
        //item.push(VendorValues[j]);
        for (var i = 0; i < newCols.length; i++) {
            item.push(result[VendorValues[j]][dic[newCols[i]]] || "");
        }
        var newArray = item.reduce(function(a, b) {
            if (typeof b == "number") {
                return a + b
            } else {
                b = 0;
                return a + b
            }
        }, 0);
        item.push(newArray);
        for (var i = 0; i < item.length - 1; i++) {
            if (item[i] != 0) {
                item[i] = item[i] / newArray;
            } else {
                ""
            }
        }
        item.unshift(VendorValues[j]);
        ret.push(item);
    }
    //return ret;
  Logger.log("this is the lastrow");
  Logger.log(lastRow);
    ret.push(lastRow);
    targetSheet.clear();
    targetSheet.getRange(1, 1, ret.length, ret[0].length).setValues(ret);
    Logger.log(ret);
};

//function creates a hidden vendor array
function countAvg_HiddenVendors(array){
  var vendorColIndex = getColIndexByName_data(array,VENDORS_COL_NAME);
  var gensatColIndex = getColIndexByName_data(array,GENSAT_COL_NAME);
  var a = filterarray(array,vendorColIndex,gensatColIndex);
  var item = []
  // item contains all the vanedor names
  // result = dictionary of "VendorName"={sum of average, count}
  var result = a.reduce(function(res, value) {
    if (!res[value[0]]) {
      res[value[0]] = [0,0];
      item.push([value[0]]);
    }
    res[value[0]][0] += value[1];
    res[value[0]][1] += 1;
    return res;
  }, {});

  for (var i = 0; i < item.length;i++){
    var count = result[item[i]][1] || "";
    var avg = result[item[i]][0]/count || "";
    if (count > MainThresh) {
      var thresh =  "1";
    } else {
      var thresh =  "0";
    }
    item[i].push(count,avg,thresh || "");
  }
  item.sort();
  item.unshift(["Vendor","Count","AVG GENSAT", "Above Threshold?"]);
  return item;
}

//function filters the hidden vendors data and aggregates the "other"
function EvalPerVendor(vendorData){
  var main = [];
  var other = ["Other",0];
  main.push(["Vendor","Count"]);
  for (var i=1; i < vendorData.length; i++){
    if(vendorData[i][3]== 1) {
      main.push([vendorData[i][0],vendorData[i][1]]);
    } else { 
      other[1] += vendorData[i][1];
    }
  }
  if (other[1] > MainThresh) {
    main.push(other)
  }
  main.sort(compareSecondColumn);
  return main
}

function GenSatGeneral(array,ColIndex,dic,type){
    if (type == 'number') {
      var a = filterarray(array,ColIndex,ColIndex);
    } else {
      var a = filterarray_text(array,ColIndex,ColIndex); 
    }
    var result = a.reduce(function(res, value) {
      if (!res[value[0]]) {
        res[value[0]] = 0;
      }
      res[value[0]] += 1;
      return res;
    }, {});
    var newCols = []
    for (var key in dic) {
      newCols.push([key]);
    }
    Logger.log(newCols)
    for (var i = 0; i < newCols.length;i++){
      if (result[dic[newCols[i]]] != -1){
        var count = result[dic[newCols[i]]];
      } else { 
        var count = 0;
      }
      var precent = count/a.length || "";
      newCols[i].push(precent);
    }
    newCols.unshift([array[0][ColIndex],"Count " + array[0][ColIndex]]);
    return newCols;
}

//this function filters the evaluation according to the filtercolumn and populates an outside sheet
function fillsheet(evalData,filterColumn,columnSortName,spreadsheet){
    var evaluationData = filterEvaluation(evalData, filterColumn);
    var vendorData = countAvg_HiddenVendors(evaluationData);
    var graph1 = EvalPerVendor(vendorData);
    var targetSS = SpreadsheetApp.openById(spreadsheet);
    var TAB1 = targetSS.getSheetByName(columnSortName+TAB_NAME_suffix[0]);
    var TAB2 = targetSS.getSheetByName(columnSortName+TAB_NAME_suffix[1]);
    var TAB3 = targetSS.getSheetByName(columnSortName+TAB_NAME_suffix[2]);
    var TAB4 = targetSS.getSheetByName(columnSortName+TAB_NAME_suffix[3]);
    TAB1.clear().getRange(1, 1, graph1.length, graph1[0].length).setValues(graph1);
    PieVendorCountChart(TAB1);
    var graph2 = VendorList(vendorData).FilterVendors;
    TAB2.clear().getRange(1, 1, graph2.length, graph2[0].length).setValues(graph2);
    var avgTable = graph2.map(function(item){ 
      return [item[0],item[2]];
    });
    MinMaxBarChart(TAB2,VendorList(vendorData).FilterVendors,0);
    //Shira - do I need to declare the variables again?
    var vendorColIndex = getColIndexByName_data(evaluationData,VENDORS_COL_NAME);
    var gensatColIndex = getColIndexByName_data(evaluationData,GENSAT_COL_NAME);
    var gentsatArray = filterarray(evaluationData,vendorColIndex,gensatColIndex);
    //var graph3 = getPivotArray_precent(gentsatArray, VendorList(vendorData).FilterVendorsList,dic_GenSat,targetSS.getSheets()[2]);
    getPivotArray_precent(gentsatArray, VendorList(vendorData).FilterVendorsList,dic_GenSat,TAB3);

    //targetSS.getSheets()[2].clear().getRange(1, 1, graph3.length, graph3[0].length).setValues(graph3);
    GenSatChartColors(TAB3);
    var renewalColIndex = getColIndexByName_data(evaluationData,RENEWAL_COL_NAME);
    var renewalArray = filterarray(evaluationData,vendorColIndex,renewalColIndex);
    //var graph4 = getPivotArray_precent(renewalArray, VendorList(vendorData).FilterVendorsList,dic_Renewal,targetSS.getSheets()[3]);
     getPivotArray_precent(renewalArray, VendorList(vendorData).FilterVendorsList,dic_Renewal,TAB4);
    //targetSS.getSheets()[3].clear().getRange(1, 1, graph4.length, graph4[0].length).setValues(graph4);
    RenewalChartColors(TAB4);
}

//function for KPI columns - almost the same as the hidden_vendors countavg
function countAvgKPI(filteredArray,vendorList){
  var item = [];
  // item contains all the vanedor names
  // result = dictionary of "VendorName"={sum of average, count}
  var result = filteredArray.reduce(function(res, value) {
    if (!res[value[0]]) {
      res[value[0]] = [0,0];
      item.push([value[0]]);
    }
    res[value[0]][0] += value[1];
    res[value[0]][1] += 1;
    return res;
    
  }, {});

  var finalList = []
  for (var i = 0; i < item.length;i++){
    var count = result[item[i]][1] || "";
    var avg = result[item[i]][0]/count || "";
    if (vendorList.indexOf(item[i][0])!= -1) {
      finalList.push([item[i][0], avg || ""]);
    }
  }
  return finalList;
}

function Multi_Col_Avg(ArrayData,StartCol,colNumber,targetSheet){
  var newArray = ArrayData.map(function(item){ 
    return item.slice(StartCol, StartCol+colNumber);
  });
  var result = newArray.filter(function(element) {
    return element.join("") != "";
  });
  var GraphArray = [];
  for (var i = 0 ; i < colNumber ; i++){
    var Col = result.filter(function(item){ 
      return item[i] != ""}).map(function(item){return item[i]});
    GraphArray.push([Col[1],Col.length/result.length]);
  }
  GraphArray.sort(compareSecondColumn);
  GraphArray.unshift([result[0][0],"Count " + result[0][0]]);
  targetSheet.clear();
  targetSheet.getRange(1, 1, GraphArray.length,2).setValues(GraphArray);
  var SortedNameList = GraphArray.map(function(item){ return item[0]});
  return {FilterArray: GraphArray, FilterList: SortedNameList};
};

function MultiCol_To_2DArray(ArrayData,colName,colNumber,textvalueList){
  var multi_Array = [["col1","col2"]];
  for (var i = 1 ; i <= colNumber ; i++){
    var col_index = getColIndexByName_data(ArrayData,colName+i);
    var newArray = ArrayData.map(function(item){ 
      return [textvalueList[i],item[col_index]];
    });
    multi_Array.push.apply(multi_Array, newArray);
  }
  return multi_Array;
}

//Section - graph colors

function PieVendorCountChart(sheet) {
    var chart = sheet.getCharts()[0];
    chart = chart.modify()
        .setOption('colors', ['#4473c4',	'#009cd9',	'#10cf9c',	'#7cca62',	'#a5c249',
                              '#4473c4',	'#009cd9',	'#10cf9c',	'#7cca62',	'#a5c249',
                              '#44B1E5',	'#8faadc',	'#4fcdff',	'#5ff3c9',	'#b0dfa1',	'#c9da92',
                              '#44B1E5',	'#8faadc',	'#4fcdff',	'#5ff3c9',	'#b0dfa1',	'#c9da92',
                              '#82b2e5',	'#91c6f7',	'#8adfff',	'#94f7dd',	'#cbeac0',	'#dbe7b6',
                              '#c1d8f2',	'#c8e3fb',	'#c4efff',	'#cafbed',	'#e5f4e0',	'#edf3db',
                              '#4473c4',	'#009cd9',	'#0bcfd9',	'#7cca62',	'#a5c249',	'#595959',
                              '#4473c4',	'#009cd9',	'#0bcfd9',	'#7cca62',	'#a5c249',	'#595959'])
        .build();    
    sheet.updateChart(chart);
}

function BarChart(sheet) {
    var chart = sheet.getCharts()[0];
    chart = chart.modify()
        .setOption('colors', ['#009DD9'])
        .build();    
    sheet.updateChart(chart);
}

function GenSatChartColors(sheet) {
    var chart = sheet.getCharts()[0];
    chart = chart.modify()
        .setOption('colors', ['#993300', '#ff9900', '#fee13e', '#d3e9c5', '#92d050', '#00b050'])
        .build();
    sheet.updateChart(chart);
}

function RenewalChartColors(sheet) {
    var chart = sheet.getCharts()[0];
    chart = chart.modify()
        .setOption('colors', ['#993300', '#ff9900', '#fee13e', '#92d050'])
        .build();
    sheet.updateChart(chart);
}

function NegotiationChartColors(sheet) {
    var chart = sheet.getCharts()[0];
    chart = chart.modify()
        .setOption('colors', ['#4473c4',	'#009cd9',	'#10cf9c'])
        .build();
    sheet.updateChart(chart);
}

function MinMaxBarChart(sheet,vandordata,chartIndex) {
    var chart = sheet.getCharts()[chartIndex];
    var genSatIndex = 2;
    var maxGen = 1;
    var minGen = 0;
  if (vandordata[0].length == 2) { genSatIndex = 1 };
  if (vandordata.length > 1) {
    maxGen = Math.ceil(vandordata[1][genSatIndex]*10)/10;
    minGen = (Math.round(vandordata[vandordata.length-1][genSatIndex]*10)-1)/10;
    if (minGen < 0) {
      minGen = 0
    }
  }
    chart = chart.modify()
    .setOption('hAxis.viewWindow', {min: minGen, max: maxGen})
        //.setOption('hAxis.viewWindow', {max: maxGen})
        .build();
    sheet.updateChart(chart);
}